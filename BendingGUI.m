function varargout = BendingGUI(varargin)
% BENDINGGUI MATLAB code for BendingGUI.fig
%      BENDINGGUI, by itself, creates a new BENDINGGUI or raises the existing
%      singleton*.
%
%      H = BENDINGGUI returns the handle to a new BENDINGGUI or the handle to
%      the existing singleton*.
%
%      BENDINGGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in BENDINGGUI.M with the given input arguments.
%
%      BENDINGGUI('Property','Value',...) creates a new BENDINGGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before BendingGUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to BendingGUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help BendingGUI

% Last Modified by GUIDE v2.5 05-Feb-2016 02:51:00

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @BendingGUI_OpeningFcn, ...
                   'gui_OutputFcn',  @BendingGUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before BendingGUI is made visible.
function BendingGUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to BendingGUI (see VARARGIN)

set(handles.popupmenu1, 'Value', 2);
set(handles.popupmenu3, 'Value', 1);
set(handles.uitable1, 'Data', {'1' '-1';'' '';'' '';'' '';'' '';'' '';'' '';'' ''});
set(handles.uitable2, 'Data', {'' '';'' '';'' '';'' '';'' '';'' '';'' '';'' ''});
set(handles.uitable3, 'Data', {'' '';'' '';'' '';'' '';'' '';'' '';});
set(handles.uitable5, 'Data', cell(8,2), 'ColumnName', {'Kraft', 'Moment'});
grid(handles.axes4)

% Choose default command line output for BendingGUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes BendingGUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = BendingGUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

setappdata(0, 'interrupt', 1);
set(hObject, 'Enable', 'off')
set(handles.pushbutton4, 'Enable', 'on');
guidata(hObject, handles);


function editTime_Callback(hObject, eventdata, handles)
% hObject    handle to editTime (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editTime as text
%        str2double(get(hObject,'String')) returns contents of editTime as a double

if ~isnan(str2double(get(handles.edit8, 'String')))
   steps = str2double(get(hObject, 'String'))/str2double(get(handles.edit8, 'String')); 
   set(handles.text24, 'String', num2str(steps, '%.0f'));
end


% --- Executes during object creation, after setting all properties.
function editTime_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editTime (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in radiobutton2.
function radiobutton2_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton2

% Dynamische Ansicht aktivieren
setappdata(0, 'interrupt', 1);
set(handles.uipanel6, 'Visible', 'off');
set(handles.uipanel8, 'Visible', 'off');
set(handles.uipanel9, 'Visible', 'on');
set(handles.dynPan, 'Visible', 'on');
set(handles.edit5, 'Enable', 'on');
set(handles.text14, 'ForegroundColor', [0 0 0]);
set(handles.text26, 'String', '');
set(handles.text27, 'Visible', 'on');
set(handles.text28, 'Visible', 'on');
set(handles.pushbutton4, 'Enable', 'on');
set(handles.pushbutton3, 'Visible', 'on');

ax = {'axes2', 'axes3'};
for i = 1:length(ax)
    cla(handles.(ax{i}),'reset')
    set(handles.(ax{i}), 'Box', 'on');
end

set(handles.text15, 'String', 'Streckenlast als Funktion f(x,t)');
set(handles.text8, 'String', 'Kr�fte F(t)');
set(handles.text9, 'String', 'Momente M(t)');

handles.pushbutton4.Position(4) = 2.777;
handles.pushbutton4.Position(2) = 3.514;

% --- Executes on button press in radiobutton1.
function radiobutton1_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton1

% Statische Ansicht aktivieren
setappdata(0, 'interrupt', 1);
set(handles.dynPan, 'Visible', 'off');
set(handles.uipanel8, 'Visible', 'on');
set(handles.uipanel9, 'Visible', 'off');
set(handles.uipanel6, 'Visible', 'on');
set(handles.edit5, 'Enable', 'off');
set(handles.text14, 'ForegroundColor', [.5 .5 .5]);
set(handles.text26, 'String', '');
set(handles.text27, 'Visible', 'off');
set(handles.text28, 'Visible', 'off');
set(handles.pushbutton3, 'Visible', 'off');

ax = {'axes2', 'axes3'};
for i = 1:length(ax)
    cla(handles.(ax{i}),'reset')
    set(handles.(ax{i}), 'Box', 'on');
end

set(handles.text15, 'String', 'Streckenlast als Funktion f(x)');
set(handles.text8, 'String', 'Kr�fte F');
set(handles.text9, 'String', 'Momente M');

handles.pushbutton4.Position(4) = 4.846;
handles.pushbutton4.Position(2) = 1.461;

% --- Executes on button press in pushbutton4.
function pushbutton4_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

ax = {'axes1', 'axes2', 'axes3', 'axes4', 'axes5'};
for i = 1:length(ax)
    cla(handles.(ax{i}),'reset')
    set(handles.(ax{i}), 'Box', 'on');
    set(handles.(ax{i}), 'Color', [.94 .94 .94]);
    set(handles.(ax{i}), 'XColor', [.5 .5 .5]);
    set(handles.(ax{i}), 'YColor', [.5 .5 .5]);
end

setappdata(0, 'interrupt', 0);
set(handles.text5, 'Visible', 'off');
handles.dynamic = get(handles.radiobutton2, 'value');
handles.E = str2double(get(handles.edit2, 'string'));
handles.I = str2double(get(handles.edit3, 'string'));
handles.L = str2double(get(handles.edit4, 'string'));
handles.mu = str2double(get(handles.edit5, 'string'));
handles.q = get(handles.edit6, 'string');
handles.interval = str2double(get(handles.editTime, 'string'));
handles.dh = str2double(get(handles.edit8, 'string'));
handles.forces = get(handles.uitable1, 'Data');
handles.momentums = get(handles.uitable2, 'Data');
handles.bearings = get(handles.uitable3, 'Data');
handles.left = get(handles.popupmenu1, 'Value');
handles.right = get(handles.popupmenu3, 'Value');
handles.oneTimeLoad = get(handles.checkbox3, 'Value');
handles.energies = [get(handles.checkbox1, 'Value')...
    get(handles.checkbox2, 'Value')];

set(handles.text26, 'String', '')
set(handles.text28, 'String', '')
set(handles.uitable5, 'Data', cell(8,2), 'ColumnName', {'Kraft', 'Moment'});

errors = 0;

if handles.dynamic
    set(handles.pushbutton3, 'Enable', 'on')
    errors = sum(isnan([handles.interval handles.dh]));
    steps = length(0:handles.dh:handles.interval) - 1;
    set(handles.text24, 'String', num2str(steps));
end

if errors == 0
    set(hObject, 'Enable', 'off');
    beam(handles);
    set(handles.pushbutton3, 'Enable', 'off')
    set(hObject, 'Enable', 'on');
else
    set(handles.text5, 'Visible', 'on');
    set(handles.text5, 'string', 'Numerische Werte als Zeitoptionen verwenden.');
end

% Kleine Fehlermeldungsanimation
if strcmp(get(handles.text5, 'Visible'), 'on')
    for i = 1:2
        set(handles.text5, 'ForeGroundColor', [0 0 0]);
        pause(.5);
        set(handles.text5, 'ForeGroundColor', [.8 .3 .1]);
        pause(.5);
    end
end

guidata(hObject, handles);


% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1


% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu3.
function popupmenu3_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu3 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu3


% --- Executes during object creation, after setting all properties.
function popupmenu3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes when selected cell(s) is changed in uitable3.
function uitable3_CellSelectionCallback(hObject, eventdata, handles)
% hObject    handle to uitable3 (see GCBO)
% eventdata  structure with the following fields (see MATLAB.UI.CONTROL.TABLE)
%	Indices: row and column indices of the cell(s) currently selecteds
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton5.
function pushbutton5_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

set(handles.uitable1, 'Data', {'' '';'' '';'' '';'' '';'' '';'' '';'' ''});

% --- Executes on button press in pushbutton6.
function pushbutton6_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

set(handles.uitable2, 'Data', {'' '';'' '';'' '';'' '';'' '';'' '';'' ''});


% --- Executes on button press in pushbutton7.
function pushbutton7_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

set(handles.uitable3, 'Data', {'' '';'' '';'' '';'' '';'' '';'' ''});



function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double


% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit3_Callback(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit3 as text
%        str2double(get(hObject,'String')) returns contents of edit3 as a double


% --- Executes during object creation, after setting all properties.
function edit3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit4_Callback(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit4 as text
%        str2double(get(hObject,'String')) returns contents of edit4 as a double


% --- Executes during object creation, after setting all properties.
function edit4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit5_Callback(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit5 as text
%        str2double(get(hObject,'String')) returns contents of edit5 as a double


% --- Executes during object creation, after setting all properties.
function edit5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit6_Callback(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit6 as text
%        str2double(get(hObject,'String')) returns contents of edit6 as a double


% --- Executes during object creation, after setting all properties.
function edit6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit7_Callback(hObject, eventdata, handles)
% hObject    handle to edit7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit7 as text
%        str2double(get(hObject,'String')) returns contents of edit7 as a double


% --- Executes during object creation, after setting all properties.
function edit7_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function edit8_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in checkbox1.
function checkbox1_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox1

plots = findall(handles.uipanel9, 'type','line');

try
   if get(hObject, 'Value') == 0
       plots(2).Visible = 'off';
   else
       plots(2).Visible = 'on';
   end
catch
    % Nichts tun.
end
% handles.enerPlots(2).Visible = 'off';


% --- Executes on button press in checkbox2.
function checkbox2_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox2

plots = findall(handles.uipanel9, 'type','line');

try
   if get(hObject, 'Value') == 0
       plots(1).Visible = 'off';
   else
       plots(1).Visible = 'on';
   end
catch
    % Nichts tun.
end


% --- Executes on button press in pushbutton8.
function pushbutton8_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

set(handles.edit6, 'String', '0');



function edit8_Callback(hObject, eventdata, handles)
% hObject    handle to edit8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit8 as text
%        str2double(get(hObject,'String')) returns contents of edit8 as a double

if ~isnan(str2double(get(hObject, 'String')))
   steps = str2double(get(handles.editTime, 'String'))/str2double(get(hObject, 'String')); 
   set(handles.text24, 'String', num2str(steps, '%.0f'));
end


% --- Executes on button press in checkbox3.
function checkbox3_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox3


% --- Executes on button press in checkbox4.
function checkbox4_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox4


% --- Executes on button press in togglebutton2.
function togglebutton2_Callback(hObject, eventdata, handles)
% hObject    handle to togglebutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of togglebutton2
plots = findall(handles.uipanel9, 'type','axes');

for i = 1:3
    if get(hObject, 'Value') == 1
        try
            handles.yLims(i,:) = ylim(plots(i));
            ylim(plots(i), 'auto');
            set(plots(i),'yscale','log');
        catch
        end
    else
        try
            set(plots(i),'yscale','linear');
            ylim(plots(i), 'manual');
            ylim(plots(i), handles.yLims(i,:));
        catch
        end
    end
end
handles.yLims

guidata(hObject, handles);