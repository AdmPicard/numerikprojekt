function beam(handles)
% beam ist die Hauptfunktion der Anwendung, die von der GUI aufgerufen wird

clc

% Einlesen allgemeiner Parameter
params.L = handles.L;
params.E = handles.E;
params.I = handles.I;
params.mu = handles.mu;
params.dh = handles.dh;
params.interval = handles.interval;
params.dynamic = handles.dynamic;
params.oneTimeLoad = handles.oneTimeLoad;

% Festlegen vordefinierter Einstellungen
params.segments = 100;
params.delay = 0;
params.beta = 1/4;
params.gamma = 1/2;

%% Loesung der DGL

equationSolver(params, handles);
