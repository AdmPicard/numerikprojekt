function [ePot, eKin] = energyCalculator(left, w, u)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

ePot = 1/2*w'*left.S*w;

if ~isempty(u)
    eKin = 1/2*u'*left.M*u;
else
    eKin = 0;
end

end

