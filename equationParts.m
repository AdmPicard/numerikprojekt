function parts = equationParts(params, saved, handles, scope, t)
% equationParts stellt je nach Scope alle Komponenten oder nur eine
% Teilmenge dieser aus der Differentialgleichung bereit
%
% scope: 2 - nur rechts, 3 - rechts und links

if scope == 3
    parts.prep = prepareInput();
    parts.left = leftSide(parts.prep, params.dynamic);
    parts.right = rightSide(parts.prep, parts.left, t);
elseif scope == 2
    parts = rightSide(saved.prep, saved.left, t);
end

%% Unterfunktionen zur Berechnung von DGL Elementen aus den Eingangsdaten

%% Vorbereitung von DGL Elementen

    function prep = prepareInput()
        % Hauptknoten bestimmen, fuer Streckenlast ergaenzen, Cell Arrays
        % fuer rechte und linke Seite der DGL erzeugen
        
        % Randbedingungen einlesen und ggf. Knoten hinzufuegen
        nodes.bounds = [];
        if handles.left > 1
            nodes.bounds = [nodes.bounds;0];
        end
        if handles.right > 1
            nodes.bounds = [nodes.bounds;params.L];
        end
        
        % Positionen der Kraefte, Momente und Lager einlesen
        fields = {'forces', 'momentums', 'bearings'};
        for i = 1:numel(fields)
            nodes.(fields{i}) = str2double(handles.(fields{i})(:,1));
            nodes.(fields{i}) = nodes.(fields{i})(isfinite(nodes.(fields{i})));
        end
        
        % Streckenlast vorbereiten und falls ungleich Null Zusatzknoten
        % hinzufuegen
        try
            prep.q = str2func(strcat('@(x,t)', handles.q));
        catch
            set(handles.text5, 'Visible', 'on');
            set(handles.text5, 'string', 'Streckenlast muss eine Funktion q(x,t) sein.');
            disp('Fehler beim Einlesen der Streckenlast');
            prep.q = @(x,t) 0;
        end
        if prep.q(1,1) ~= 0
            nodes.additional = linspace(0, params.L, params.segments)';
        else
            nodes.additional = [];
        end
        
        % Knoten zusammenfassen, sortieren und Duplikate eliminieren
        nodes.main = sort(cat(1, nodes.bounds, nodes.forces, ...
            nodes.momentums, nodes.bearings, params.L, 0));
        nodes.all = sort(cat(1, nodes.main, nodes.additional));
        nodes.mainU = unique([nodes.main; params.L; 0]);
        nodes.allU = unique(nodes.all);
        nodes.numMain = length(nodes.mainU);
        nodes.numAll = length(nodes.allU);
        
        nodes.numFM = sum(length(unique(cat(1,nodes.forces, nodes.momentums))));
        
        prep.nodes = nodes;
        
        % Allgemeines Cell Array mit Function Handlern fuer spaeteren
        % Vektor mit eingepraegten Kraeften und Momenten erzeugen
        
        prep.fmCells = cell(nodes.numFM, 2);
        
        index = 1;
        fields = {'forces', 'momentums'};
        for i = 1:numel(fields)
            handles.(fields{i})(any(cellfun(@isempty,handles.(fields{i})),2),:) = [];
            for id = 1:size(handles.(fields{i}),1)
                prep.fmCells{index,1} = find(nodes.allU==str2double(handles.(fields{i}){id,1}),1);
                if i == 1
                    prep.fmCells{index,2} = {str2func(strcat('@(t)',handles.(fields{i}){id,2}));@(t) 0};
                else
                    prep.fmCells{index,2} = {@(t) 0;str2func(strcat('@(t)',handles.(fields{i}){id,2}))};
                end
                index = index + 1;
            end
        end
    end
    
%% Matrizen der fuer linke Seite der DGL erzeugen

    function left = leftSide(prep, mode)
        
        %% Matrix der Zwangskraefte erzeugen
        
        % Dictionaries zur Uebersetzung von Lagerinfos
        bearingNames = {'',' ', 'Eingespannt', 'Festlager', 'Verschiebbar'};
        bearingMatrices = {[], [], [1 0;0 1], [1;0], [0;1]};
        bearingTypes = containers.Map(bearingNames, bearingMatrices);
        
        % Randbedingungen wie Lagerinfos als Cell Array formatieren
        boundsCells = {1 bearingTypes(bearingNames{handles.left+1})
            prep.nodes.numAll bearingTypes(bearingNames{handles.right+1})};
        
        % Lagerinformationen aus Eingabemaske lesen, Position in Zahl
        % konvertieren und Lagertyp ersetzen
        handles.bearings(:,2) = cellfun(@(x) bearingTypes(x), ...
            handles.bearings(:,2), 'UniformOutput', 0);
        handles.bearings(:,1) = cellfun(@(x) find(prep.nodes.allU == ...
            str2double(x), 1), handles.bearings(:,1), 'UniformOutput', 0);
        
        % Randbedingungen und Lager zusammenfuegen
        allBearings = [boundsCells; handles.bearings];
        
        % Leere Lagerinformationen entfernen
        allBearings(cellfun(@(x) isempty(x), allBearings(:,2)),:)=[];
        allBearings(cellfun(@(x) isempty(x), allBearings(:,1)),:)=[];
        
        % Lagerliste sortieren, Lagerreaktionen zaehlen
        degrees = sum(cell2mat(allBearings(:,2)'),2);
        try
            bearings.degrees = degrees(2);
            bearings.degreesV = degrees(1);
        catch
            bearings.degrees = 0;
            bearings.degreesV = 0;
        end
        allBearings = sortrows(allBearings, 1);
        
        % Matrix mit einsortierten Lagertypen erzeugen
        matrix = zeros(prep.nodes.numAll*2, bearings.degrees);
        index = 1;
        for i = 1:size(allBearings, 1)
            el = allBearings(i,:);
            matrix(el{1}*2-1:el{1}*2, index:index+size(el{2},2)-1) = el{2};
            index = index + size(el{2},2);
        end
        
        left.eMatrix = matrix;
        left.allBearings = allBearings;
        left.degrees = bearings.degrees;
        left.degreesV = bearings.degreesV;
        
        %% Steifigkeitsmatrix und ggf. Massenmatrix erzeugen
        
        left.S = zeros(prep.nodes.numAll*2);
        
        if mode == 1
            left.M = zeros(prep.nodes.numAll*2);
        end
        
        for i = 1:prep.nodes.numAll-1
            h = prep.nodes.allU(i+1) - prep.nodes.allU(i);
            id = 1 + 2*(i - 1);
            
            % Steifigkeitsmatrix
            S_seg = params.E*params.I/h^3*[12   6*h     -12     6*h
                                           0    4*h^2   -6*h    2*h^2
                                           0    0       12      -6*h
                                           0    0       0       4*h^2];
            
            S_seg = triu(S_seg, 1)'+S_seg;
            left.S(id:id+3,id:id+3) = left.S(id:id+3,id:id+3) + S_seg;
            
            % Massenmatrix
            if mode == 1
                M_seg = params.mu * h/420 * [156        22*h    54      -13*h
                                             22*h       4*h^2   13*h    -3*h^2
                                             54         13*h    156     -22*h
                                             -13*h      -3*h^2  -22*h   4*h^2];

                left.M(id:id+3,id:id+3) = left.M(id:id+3,id:id+3) + M_seg;
            end
        end
        
        % Steifigkeitsmatrix erweitern, ggf. Massenmatrix auffuellen
        
        left.Se = [left.S left.eMatrix
                   left.eMatrix' zeros(size(left.eMatrix,2))];
        
        if mode == 1
            left.Me = zeros(length(left.Se));
            left.Me(1:length(left.M),1:length(left.M)) = left.M;
        end
    end

%% Rechte Seite der DGL mit Streckenlast und eingepraegten Kraeften erstellen

    function right = rightSide(saved, left, t)
        
        %% Streckenlast auswerten
        
        right.qVec = zeros(length(left.Se),1);     
        for k = 0:length(saved.nodes.allU)-2
            nodes = saved.nodes.allU;
            h = nodes(k+2) - nodes(k+1);
            id = 1 + 2*k;
            ln = nodes(k+1);
            rn = nodes(k+2);
            q_seg = h/60*[21*saved.q(ln,t)+9*saved.q(rn,t)
                h*(3*saved.q(ln,t)+2*saved.q(rn,t))
                9*saved.q(ln,t)+21*saved.q(rn,t)
                -h*(2*saved.q(ln,t)+3*saved.q(rn,t))];
            right.qVec(id:id+3) = right.qVec(id:id+3) + q_seg;
        end
        
        %% Zwangskraefte auswerten und Vektor erzeugen
        
        right.fmVec = zeros(length(left.Se),1);
        for i = 1:size(saved.fmCells,1)
            el = saved.fmCells(i,:);
            el{2} = cellfun(@(c) c(t),el{2});
            right.fmVec(el{1}*2-1:el{1}*2) = right.fmVec(el{1}*2-1:el{1}*2) + el{2}; 
        end
    end

end

