function equationSolver(params, handles)
% equationSolver loest die Differentialgleichung je nach Vorgabe statisch
% oder dynamisch

%% Abfangen fehlerhafter Eingaben

try
    parts = equationParts(params, 0, handles, 3, 0);
    errors = 0;
    bearingNodes = [parts.prep.nodes.bounds; parts.prep.nodes.bearings];
    if length(bearingNodes) > length(unique(bearingNodes))
        errors = 1;
        set(handles.text5, 'Visible', 'on');
        set(handles.text5, 'string', 'Lagerpositionen mehrfach vergeben.');
    end
    if parts.left.degrees < 1 && parts.left.degreesV < 2
        set(handles.text5, 'Visible', 'on');
        set(handles.text5, 'string', 'Das System ist unterbestimmt (drehbar).');
        errors = 1;
    elseif parts.left.degrees > 0 && parts.left.degreesV < 1
        set(handles.text5, 'Visible', 'on');
        set(handles.text5, 'string', 'Das System ist unterbestimmt (verschiebbar).');
        errors = 1;
    end
    if parts.left.degrees < 1 && parts.left.degreesV < 1
        set(handles.text5, 'Visible', 'on');
        set(handles.text5, 'string', 'Das System ist unterbestimmt (drehbar, verschiebbar).');
        errors = 1;
    end
    if max(parts.prep.nodes.allU) > params.L || min(parts.prep.nodes.allU) < 0
        set(handles.text5, 'Visible', 'on');
        set(handles.text5, 'string', 'Elementknoten au�erhalb der Balkens vorhanden.');
        errors = 1;
    end
catch
    set(handles.text5, 'Visible', 'on');
    set(handles.text5, 'string', 'Fehlerhafte Eingaben vorhanden.');
    errors = 1;
end

%% Bilden und Loesen der Differentialgleichung

if params.dynamic == 0 && errors == 0
    
    % Statische Berechnung
    startTime = clock;
    w = parts.left.Se \ (parts.right.qVec+parts.right.fmVec);
    reactionParser(parts.left.allBearings, w, handles);
    curves = curveGenerator(w, parts.prep.nodes);
    ylims = ylimCalculator({curves.allBending, curves.allMomentum, curves.allForce});
    solutionPlotter(curves, params, parts.prep.nodes, handles, ylims);
    
    % Energie anzeigen
    [enerPot,~] = energyCalculator(parts.left, ...
        w(1:length(parts.left.S),1), []);
    set(handles.text32, 'String', num2str(enerPot, '%.3f'));
    
    % Berechnungsdauer anzeigen
    elapsedTime = num2str(etime(clock, startTime));
    set(handles.text26, 'String', strcat(elapsedTime, ' s'));

elseif errors == 0
    
    % Dynamische Berechnung    
    u = [zeros(length(parts.right.qVec),1) zeros(length(parts.right.qVec),1)...
        zeros(length(parts.right.qVec),1)];
    ust = zeros(length(parts.right.qVec), 2);
    dh = params.dh;
    frames = 0:dh:handles.interval;
    
    % Zeiberechnungen (Schritt, Gesamt, Prognose)
    if length(frames) > 200
        progTimes = round(0:10/dh:length(frames));
        span = 10/dh;
    else
        progTimes = frames;
        span = 1;
    end
    index = 1;
    timerList = zeros(length(progTimes),1);
    startTime = clock;
    timer = clock;
    
    enerTimeNodes = zeros(1,length(frames));
    enerPot = zeros(1,length(frames));
    enerKin = zeros(1,length(frames));
    plotLims = {[99999999 -99999999],[99999999 -99999999],[99999999 -99999999],[99999999 -99999999]};
    
    right = equationParts(params, parts, handles, 2, frames(1));

    for t = 1:length(frames)
        
        if any(t == progTimes) && t > 1
            timerList(index) = etime(clock, timer)/span;
            timer = clock;
            index = index +1;
        end
        everyTime = clock;
        
        % Neuberechnen der rechten Seite fuer jeden Zeitschritt
        if params.oneTimeLoad
            right = equationParts(params, parts, handles, 2, frames(t));
        elseif t == 2
            right.fmVec = right.fmVec * 0;
            right.qVec = right.qVec * 0;
        end
        
        % Anwendung Newmark-Algorithmus
        ust(:,1) = u(:,1) + u(:,2) * dh + (1/2 - params.beta)*u(:,3)*dh^2;
        ust(:,2) = u(:,2) + (1 - params.gamma)*u(:,3)*dh;
        u(:,3) = ((parts.left.Me + params.beta*dh^2*parts.left.Se))\...
            (right.qVec+right.fmVec-parts.left.Se*ust(:,1));
        
        u(:,1) = ust(:,1) + params.beta * u(:,3)*dh^2;
        u(:,2) = ust(:,2) + params.gamma*u(:,3)*dh;

        curves = curveGenerator(u(:,1), parts.prep.nodes);
        
        % Energiebetrachtung
        enerTimeNodes(t) = frames(t);
        [enerPot(t),enerKin(t)] = energyCalculator(parts.left, ...
            u(1:length(parts.left.S),1),...
            u(1:length(parts.left.S),2));
        enerT = t;
        
        if get(handles.checkbox4, 'Value') || t == length(frames) || getappdata(0, 'interrupt') == 1
            curves.enerTimeNodes = enerTimeNodes;
            curves.enerPot = enerPot;
            curves.enerKin = enerKin;
            curves.enerTot = enerPot + enerKin;
            curves.enerT = enerT;
            
            % Anpassung der ylims von Biegelinie und Energien
            fds = fieldnames(curves);
            fids = [2 16 17 18];
            
            for id = 1:length(fids)
                if max(curves.(fds{fids(id)})) > plotLims{id}(2)
                    plotLims{id}(2) = max(curves.(fds{fids(id)}));
                end
                if min(curves.(fds{fids(id)})) < plotLims{id}(1)
                    plotLims{id}(1) = min(curves.(fds{fids(id)}));
                end
            end
            
            ylims = ylimCalculator(plotLims);
            solutionPlotter(curves, params, parts.prep.nodes, handles, ylims);
            
            % Stopp-Funktion aus GUI
            interrupt = getappdata(0, 'interrupt');
            if interrupt == 1;
                set(handles.text30, 'String', '')
                break
            end
            
        end
        drawnow
        
        % Zeitinformationen
%         timerList(t) = etime(clock, timer);
        innerMean = mean(nonzeros(timerList));
        remaining = innerMean*(length(frames)-t);
        if ~isnan(remaining)
            set(handles.text30, 'String', strcat(num2str(remaining, '%.0f'), ' s'));
        end
        set(handles.text28, 'String', strcat(num2str(etime(clock, everyTime), '%.3f'), ' s'));
        set(handles.text24, 'String', strcat(num2str(t-1), ' / ', num2str(length(frames)-1)));
        pause(params.delay)
    end
    
    meanTime = num2str(mean(nonzeros(timerList)), '%.3f');
    set(handles.text28, 'String', strcat(meanTime, ' s'));
    set(handles.text26, 'String', strcat(num2str(etime(clock, startTime), '%.3f'), ' s'));
end

%% Berechnung der Kurven fuer die Plots

    function curves = curveGenerator(w, prepNode)
        % Berechnet Teilkurven zur Darstellung in den Plots
        
        nodes = prepNode.allU;
        mainNodes = prepNode.mainU;
        
        curves.allX = [];
        curves.allBending = [];
        curves.allMomentum = [];
        curves.allForce = [];
        curves.nodesXF = [];
        curves.nodesXM = [];
        curves.nodesW = [];
        curves.nodesF = [];
        curves.nodesM = [];
        curves.mainNodesW = [];
        curves.mainNodesM = [];
        curves.mainNodesF = [];
        
        for k = 0:length(nodes)-2
            
            % Aktuelles Segment des Loesungsvektors waehlen
            currentW = w(1+2*k:4+2*k);
            jumpF = 0;
            
            % Untergruppe von Punkten fuer spaeteren Plot erzeugen
            curves.subnodes = linspace(nodes(k+1), nodes(k+2), 50);
            h = nodes(k+2) - nodes(k+1);
            
            % Formpolynome bestuecken
            formPolynoms = [[2/h^3, -3/h^2, 0, 1],  [12/h^3, -6/h^2]
                [1/h^2, -2/h, 1, 0],    [6/h^2, -4/h]
                [-2/h^3, 3/h^2, 0, 0],  [-12/h^3, +6/h^2]
                [1/h^2, -1/h, 0, 0],    [6/h^2, -2/h]];
            
            % Biegelinienverlauf berechnen
            curves.allX = [curves.allX curves.subnodes];
            poly.bending = sum(formPolynoms(:,1:4).*repmat(currentW,1,4),1);
            curves.subBending = polyval(poly.bending, curves.subnodes- nodes(k+1));            
            curves.allBending = [curves.allBending curves.subBending];
            curves.nodesW = [curves.nodesW curves.subBending(1)];
            
            if params.dynamic == 0
                
                % Im statischen Fall Moment- und Querkraftverlauf berechnen
                poly.momentum = params.E*params.I*sum(formPolynoms(:,5:6).*repmat(currentW,1,2),1);
                poly.force = polyder(poly.momentum, 1);
                
                curves.subMomentum = polyval(poly.momentum, [0 curves.subnodes(end)-nodes(k+1)]);
                curves.subForce = polyval(poly.force, [0 curves.subnodes(end)-nodes(k+1)]);

                curves.allMomentum = [curves.allMomentum curves.subMomentum];
                curves.nodesM = [curves.nodesM curves.subMomentum];
                curves.nodesXM = [curves.nodesXM curves.subnodes([1 end])];
                
                % Kurvenglaettung fuer Querkraftverlauf fuegt nur bei
                % signifikanten Spruengen zusaetzliche Knoten ein (sonst
                % Treppeneffekt bei hoeheren, aber stetigen, Anstiegen)
                if k > 0
                    combinedForce = [curves.allForce(end) curves.subForce(1)];
                    jumpF = abs((combinedForce(1)-combinedForce(2))/median(combinedForce));
                end
                
                if jumpF > 10^5 || (length(nodes) < 100 && (k == 0 || k == length(nodes)-2))
                    curves.allForce = [curves.allForce curves.subForce];
                    curves.nodesF = [curves.nodesF curves.subForce];
                    curves.nodesXF = [curves.nodesXF curves.subnodes([1 end])];
                else
                    curves.allForce = [curves.allForce curves.subForce(1)];
                    curves.nodesF = [curves.nodesF curves.subForce(1)];
                    curves.nodesXF = [curves.nodesXF curves.subnodes(1)];
                end
                
                if any(nodes(k+1) == mainNodes)
                    curves.mainNodesM = [curves.mainNodesM curves.subMomentum(1)];
                    curves.mainNodesF = [curves.mainNodesF curves.subForce(1)];
                end
            end
            
            if any(nodes(k+1) == mainNodes)
                curves.mainNodesW = [curves.mainNodesW curves.subBending(1)];
            end
        end
        
        % Rechte Randpunkte ergaenzen
        curves.nodesW = [curves.nodesW curves.subBending(end)];
        curves.mainNodesW = [curves.mainNodesW curves.subBending(end)];
        if params.dynamic == 0
            curves.mainNodesM = [curves.mainNodesM curves.nodesM(:,end)];
            curves.mainNodesF = [curves.mainNodesF curves.nodesF(:,end)];
        end
    end

%% ylim Berechnung

    function ylims = ylimCalculator(yCells)
        % Funktion berechnet ylims fuer eine angenehme Darstellung
        
        ylims = zeros(length(yCells),2);
        fc = [3/10 4/10 4/10];
        
        if params.dynamic == 1
            fc = [3/10 1/10 1/10 1/10];
        end
        
        for ind = 1:length(yCells)
            y2 = max(yCells{ind});
            y1 = min(yCells{ind});
            dy = y2 - y1;
            if abs(dy) > .0001 %|| params.dynamic == 1 
                ylims(ind,:) = sort([y1-dy*fc(ind) y2+dy*fc(ind)]);
            elseif y1 ~= 0
                ylims(ind,:) = sort([0-y1*fc(ind) y2+y2*fc(ind)]);
            else
                ylims(ind,:) = [-1 1];
            end
        end
    end

end

