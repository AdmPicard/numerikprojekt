function reactionParser (mat, w, handles)
% reactionParser Liest Lagerreaktionen aus dem Ergebnisvektor und stellt
% diese dar

tab = cell(2,8);
mat = cellfun(@(x) sum(x,2)', mat(:,2), 'UniformOutput', 0);
mat = cell2mat(mat)';
reactions = sum(mat(:));
w = w(end-reactions+1:end);
pos = 1;
for s = 1:size(mat,2)
    for z = 1:size(mat,1)
        if mat(z,s) == 1
            tab{z,s} = w(pos);
            pos = pos + 1;
        end
    end
end

tab = cellfun(@(x) num2str(x,'%.2g'), tab, 'UniformOutput', 0);
set(handles.uitable5, 'Data', tab', 'ColumnName', {'Kraft', 'Moment'});

end

