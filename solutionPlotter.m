function solutionPlotter(curves, params, nodes, handles, ylims)
% solutionPlotter Gibt Kurven mit zuvor erzeugten Punkten aus

%% Plot der Biegelinie und ggf. der Belastungsverlaeufe

plotInfoSet = {{'allX', 'allBending', 'mainNodesW', [.5 .5 .5], 5, ' m'}
               {'nodesXM', 'nodesM', 'mainNodesM', [0 0.4470 0.7410], 1, ' Nm'}
               {'nodesXF', 'nodesF', 'mainNodesF', [0 0.4470 0.7410], 1, ' N'}};

if ~params.dynamic
    scope = 1:3;
else
    scope = 1;
end

for i = scope
    thisAxes = handles.(strcat('axes',num2str(i)));
    plot(thisAxes, [0 params.L], [0,0], 'color', [.4 .4 .4])
    hold(thisAxes, 'on')
    xlim(thisAxes, [-.1*params.L 1.1*params.L])
    ylim(thisAxes, ylims(i,:))
    plot(thisAxes, curves.(plotInfoSet{i}{1}), curves.(plotInfoSet{i}{2}),...
        'color', plotInfoSet{i}{4}, 'LineWidth', plotInfoSet{i}{5})
    scatter(thisAxes, nodes.mainU, curves.(plotInfoSet{i}{3})(1,:), 20,...
        'MarkerEdgeColor', [.5 .5 .5], 'MarkerFaceColor', 'white')
    allY = curves.(plotInfoSet{i}{3})(1,:);
    if handles.dynamic == 0
        for n = 1:length(nodes.mainU)
            text(.826/params.L*nodes.mainU(n)+.085, .12, strcat('\fontsize{8em}', ...
                num2str(allY(n), '%.2f'), plotInfoSet{i}{6}),'Parent', thisAxes, ...
                'HorizontalAlignment', 'center', 'Interpreter', 'tex', ...
                'units','normalized');
        end
    end
    hold(thisAxes, 'off')
end
    
%% Darstellung des Energieverlaufs

if params.dynamic == 1 && curves.enerT > 1
    [ax,p1,p2] = plotyy(handles.axes5, curves.enerTimeNodes(1:curves.enerT), curves.enerPot(1:curves.enerT),...
        curves.enerTimeNodes(1:curves.enerT), curves.enerKin(1:curves.enerT));
    xlim(ax(1), [-.05*params.interval 1.05*params.interval])
    xlim(ax(2), [-.05*params.interval 1.05*params.interval])  
    handles.axes5.Box = 'off';
    
    % Aus-/Einblenden der Einzelenergien waehrend der Berechnung
    if get(handles.checkbox1, 'Value') == 0
        p1.Visible = 'off';
    else
        p1.Visible = 'on';
    end
    if get(handles.checkbox2, 'Value') == 0
        p2.Visible = 'off';
    else
        p2.Visible = 'on';
    end
    
    p1.Color = [0 0.4470 0.7410];
    ax(1).YColor = [0 0.4470 0.7410];
    
    % Gesamtenergie
    plot(handles.axes4, curves.enerTimeNodes(2:curves.enerT), ...
        curves.enerTot(2:curves.enerT), 'Color', [.3 .3 .3]);
    xlim(handles.axes4, [-.05*params.interval 1.05*params.interval])
    grid(handles.axes4);
    
    if get(handles.togglebutton2, 'Value') == 1
        ylim(ax(1), [0 ylims(2,2)])
        ylim(ax(2), [0 ylims(3,2)])
        ylim(handles.axes4, [0 ylims(4,2)])
        set(ax(1),'yscale','log');
        set(ax(2),'yscale','log');
        set(handles.axes4,'yscale','log');
    else
        set(ax(1),'yscale','linear');
        set(ax(2),'yscale','linear');
        set(handles.axes4,'yscale','linear');
        ylim(ax(1), ylims(2,:))
        ylim(ax(2), ylims(3,:))
        ylim(handles.axes4, ylims(4,:))
    end
end

end

